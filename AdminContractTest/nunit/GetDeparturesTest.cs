﻿using Ferry.Admin.Contract;
using Ferry.Admin.Contract.Dto;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminContractTest.nunit
{
    [TestFixture]
    public class GetDeparturesTest
    {
        //Use whatever implementation of IAdminController you wish to test.
        private IAdminController _adminContract = null;

        [Test]
        public void GetDepartures()
        {
            // Arrange
            DateTime startDate = new DateTime(2014, 01, 01);

            // Act
            List<DepartureDTO> departures = _adminContract.GetDepartures(startDate).ToList<DepartureDTO>();

            // Assert
            Assert.IsNotNull(departures, "Departures cannot be null.");
            Assert.AreEqual(9, departures.Count, "Wrong number of departures.");
        }

        [Test]
        public void GetDeparturesNoDeparturesTest()
        {
            // Arrange
            //No departures fo this date.
            DateTime startDate = new DateTime(2016, 01, 01);
            // Act
            List<DepartureDTO> departures = _adminContract.GetDepartures(startDate).ToList<DepartureDTO>();

            // Assert
            Assert.IsNotNull(departures, "Departures cannot be null.");
            Assert.AreEqual(0, departures.Count, "Wrong number of departures.");
        }
    }
}
