﻿using Ferry.Admin.Contract;
using Ferry.Admin.Contract.Dto;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminContractTest.nunit
{
    [TestFixture]
    public class ShowReservationsTest
    {
        //Use whatever implementation of IAdminController you wish to test.
        private IAdminController _adminContract = null; 

        [Test]
        public void ShowReservations()
        {
            // Arrange
            DepartureDTO departure = new DepartureDTO() { DepartureTime = new DateTime(2014, 01, 01), FerryConfigId = 1, RouteId = 1 };

            // Act
            //To list
            List<ReservationDTO> reservations = _adminContract.ShowReservations(departure).ToList<ReservationDTO>();

            // Assert
            Assert.IsNotNull(reservations, "Reservations cannot be null.");
            Assert.AreEqual(2, reservations.Count, "Wrong number of reservations.");
            Assert.IsTrue(reservations.Exists(r => r.Booker == "John" && r.ReservationId == "1abc" && r.NumberOfPassengers == 1 && r.Price == 50));
            Assert.IsTrue(reservations.Exists(r => r.Booker == "Bent" && r.ReservationId == "2abc" && r.NumberOfPassengers == 2 && r.Price == 100));
        }

        [Test]
        public void NoReservations()
        {
            // Arrange
            //No reservations for date.
            DepartureDTO departure = new DepartureDTO() { DepartureTime = new DateTime(2016, 01, 01), FerryConfigId = 1, RouteId = 1 };

            // Act
            List<ReservationDTO> reservations = this._adminContract.ShowReservations(departure).ToList<ReservationDTO>();

            // Assert
            Assert.IsNotNull(reservations, "Reservations cannot be null.");
            Assert.AreEqual(0, reservations.Count, "Wrong number of reservations. Must be zero");
        }
    }
}
