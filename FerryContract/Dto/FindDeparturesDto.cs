﻿using System;

namespace Ferry.Customer.Contract.Dto
{
    public class FindDeparturesDto
    {
        public HarborDto Origin { get; set; }
        public HarborDto Destination { get; set; }
        public VehicleDto Vehicle { get; set; }
        public int NumberOfPassengers { get; set; }
        public DateTime DateOfDeparture { get; set; }
    }
}
