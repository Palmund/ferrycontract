﻿using System;

namespace Ferry.Customer.Contract.Dto
{
    public class TripDto
    {
        public int Id { get; set; }
        public HarborDto Origin { get; set; }
        public HarborDto Destination { get; set; }
        public DateTime TimeOfDeparture { get; set; }
        public decimal Price { get; set; }
        public TimeSpan Duration { get; set; }
    }
}