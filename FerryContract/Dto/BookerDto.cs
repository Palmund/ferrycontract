﻿namespace Ferry.Customer.Contract.Dto
{
    public class BookerDto
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
