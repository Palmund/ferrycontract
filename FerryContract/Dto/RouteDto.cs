﻿using System;

namespace Ferry.Customer.Contract.Dto
{
    public class RouteDto
    {
        public int Id { get; set; }
        public HarborDto Destination { get; set; }
        public HarborDto Origin { get; set; }
        public TimeSpan Duration { get; set; }

    }
}