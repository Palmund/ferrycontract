﻿namespace Ferry.Customer.Contract.Dto
{
    public class ReservationDto
    {
        public string ReservationId { get; set; }
        public BookerDto Booker { get; set; }
        public TripDto OutboundTrip { get; set; }
        public TripDto ReturnTrip { get; set; }
        public decimal Price { get; set; }
        public VehicleDto Vehicle { get; set; }
        public int NumberOfPassengers { get; set; }
    }
}