﻿namespace Ferry.Customer.Contract.Dto
{
    public class VehicleDto
    {
        public string RegNumber { get; set; }

        //length is messured in cm
        public int Length { get; set; }
        //Weight is messured in kg
        public double Weight { get; set; }
    }
}