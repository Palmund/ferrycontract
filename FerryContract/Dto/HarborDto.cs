﻿namespace Ferry.Customer.Contract.Dto
{
    public class HarborDto
    {
        public int Id { get; set; }
        //We are not sure if this class needs a id, its up to the backend people
        public string Name { get; set; }
    }
}