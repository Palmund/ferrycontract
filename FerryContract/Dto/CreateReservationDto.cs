﻿namespace Ferry.Customer.Contract.Dto
{
    /// <summary>
    /// This class represents a reservation with an outbound trip and an optionally return trip.
    /// The number of passengers and the chosen vehicle is the same for both the outbound and
    /// return trip. If the customer wants a different selection for any trip, s/he has to create
    /// a separate reservation for it.
    /// </summary>
    public class CreateReservationDto
    {
        public BookerDto Booker { get; set; }
        public TripDto OutboundTrip { get; set; }
        public TripDto ReturnTrip { get; set; }
        public VehicleDto Vehicle { get; set; }
        public int NumberOfPassengers { get; set; }
    }
}