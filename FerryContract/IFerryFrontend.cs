﻿using System.Collections.Generic;
using System.Security.AccessControl;
using Ferry.Customer.Contract.Dto;

namespace Ferry.Customer.Contract
{
    public interface IFerryFrontend
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns>Returns a list of all vehicles</returns>
        IEnumerable<VehicleTypeDto> GetVehicleTypes();

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Returns a list of all routes in the system</returns>
        IEnumerable<RouteDto> GetAllRoutes();

        /// <summary>
        ///     Finds a list of departures for the given params
        /// </summary>
        /// <param name="search"></param>
        /// <returns>Returns list of departures</returns>
        IEnumerable<TripDto> FindDepartures(FindDeparturesDto search);

        /// <summary>
        ///     Reservation is confirmed.
        ///     If returnTrip is null there is no return trip.
        /// </summary>
        /// <param name="reservation">A compilation of the booker and outbound and inbound trips.</param>
        /// <returns>Null if something went wrong</returns>
        ReservationDto CreateReservation(CreateReservationDto reservation);

        /// <summary>
        ///     Finds bookers reservation, for updating or deleting
        /// </summary>
        /// <param name="reservationNumber"></param>
        /// <param name="name"></param>
        /// <returns>the reservation found else Null</returns>
        ReservationDto FindReservation(string reservationNumber, string name);

        /// <summary>
        ///     Updates the reservation
        /// </summary>
        /// <param name="updatedReservation"></param>
        /// <returns>returns updated reservation</returns>
        ReservationDto UpdateReservation(ReservationDto updatedReservation);

        /// <summary>
        ///     Deletes the reservation
        /// </summary>
        /// <param name="reservation"></param>
        /// <returns>true if reservation is deleted</returns>
        bool DeleteReservation(ReservationDto reservation);
    }
}