﻿using System;
using System.Linq;
using Ferry.Customer.Contract;
using Ferry.Customer.Contract.Dto;
using Ferry.Customer.Contract.Eto;
using NUnit.Framework;

namespace Ferry.Customer.Test
{
    [TestFixture]
    public class CustomerFerryFrontendUnitTest
    {
        private readonly IFerryFrontend _manager = null;

        [Test]
        public void GetVehicleTypes()
        {
            var vehicles = _manager.GetVehicleTypes();
            //Car & Truck Expected
            Assert.AreEqual(2, vehicles.Count(), "Expected exactly two vehicle types");
        }

        [Test]
        public void GetAllRoutesTest()
        {
            var routes = _manager.GetAllRoutes();
            Assert.AreEqual(4, routes.Count(), "Expected exactly 4 routes");
        }

        [Test]
        public void DeleteReservation()
        {
            // Arrange + Act
            var originalReservation = _manager.FindReservation("123456789", "Svend Bent");
            var reservationDeleted = _manager.DeleteReservation(originalReservation);

            // Assert
            Assert.IsTrue(reservationDeleted, "Reservation was not deleted");
        }

        [Test]
        public void UpdateReservation()
        {
            // Arrange
            var reservation = _manager.FindReservation("123456789", "Svend Bent");

            // Act
            reservation.NumberOfPassengers = 1;
            var updatedReservation = _manager.UpdateReservation(reservation);

            // Assert
            Assert.AreEqual(updatedReservation, reservation, "The updated reservation returned from the data layer does not match the expected");
        }

        [Test]
        public void CreateReservation()
        {
            // Arrange
            var reservation = new CreateReservationDto
            {
                OutboundTrip = new TripDto
                {
                    Origin = new HarborDto
                    {
                        Name = "Læsø"
                    },
                    Destination = new HarborDto
                    {
                        Name = "Sprogø"
                    },
                    Price = 20,
                    TimeOfDeparture = DateTime.Now,
                    Duration = TimeSpan.FromHours(1)
                },
                ReturnTrip = null,
                NumberOfPassengers = 1,
                Vehicle = null,
                Booker = new BookerDto
                {
                    Name = "Svend Bent",
                    Email = "svend@bent.dk"
                }
            };

            // Act
            var reservationDto = _manager.CreateReservation(reservation);

            // Assert
            Assert.IsNotNull(reservationDto.ReservationId, "Reservation id is null");
            Assert.AreEqual(20, reservation.OutboundTrip.Price, "The price of the outbound trip is different from 20");
            Assert.AreEqual(reservation.OutboundTrip.Price, reservationDto.Price, "The price of the total reservation does not match the price of the outbound trip");
        }

        [Test]
        [ExpectedException(typeof (SaveReservationEto))]
        public void CreateNastyReservation()
        {
            // Arrange
            var reservation = new CreateReservationDto
            {
                OutboundTrip = null,
                ReturnTrip = new TripDto
                {
                    Origin = new HarborDto
                    {
                        Name = "Læsø"
                    },
                    Destination = new HarborDto
                    {
                        Name = "Sprogø"
                    },
                    Price = 20,
                    TimeOfDeparture = DateTime.Now,
                    Duration = TimeSpan.FromHours(1)
                },
                NumberOfPassengers = 1,
                Vehicle = null,
                Booker = new BookerDto
                {
                    Name = "Svend Bent",
                    Email = "svend@bent.dk"
                }
            };

            // Act
            _manager.CreateReservation(reservation);
        }

        [Test]
        public void CreatePrettyReturnReservation()
        {
            // Arrange
            var reservation = new CreateReservationDto
            {
                OutboundTrip = new TripDto
                {
                    Origin = new HarborDto
                    {
                        Name = "Læsø"
                    },
                    Destination = new HarborDto
                    {
                        Name = "Sprogø"
                    },
                    Price = 20,
                    TimeOfDeparture = DateTime.Now,
                    Duration = TimeSpan.FromHours(1)
                },
                ReturnTrip = new TripDto
                {
                    Origin = new HarborDto
                    {
                        Name = "Sprogø"
                    },
                    Destination = new HarborDto
                    {
                        Name = "Læsø"
                    },
                    Price = 40,
                    TimeOfDeparture = DateTime.Now,
                    Duration = TimeSpan.FromHours(1)
                },
                NumberOfPassengers = 1,
                Vehicle = null,
                Booker = new BookerDto
                {
                    Name = "Svend Bent",
                    Email = "svend@bent.dk"
                }
            };

            // Act
            var reservationDto = _manager.CreateReservation(reservation);

            // Assert
            Assert.IsNotNull(reservationDto.ReservationId, "Reservation id is null");
            Assert.AreEqual(60, reservationDto.Price, "The price of the reservation is different from 60");
            Assert.AreEqual(reservation.OutboundTrip, reservationDto.OutboundTrip, "The price of the outbound trip does not match the expected value");
            Assert.AreEqual(reservation.ReturnTrip, reservationDto.ReturnTrip, "The price of the return trip does not match the expected value");
        }

        [Test]
        [ExpectedException(typeof (SaveReservationEto))]
        public void CreateNastyReturnReservation()
        {
            // Arrange
            var reservation = new CreateReservationDto
            {
                OutboundTrip = new TripDto
                {
                    Origin = new HarborDto
                    {
                        Name = "Læsø"
                    },
                    Destination = new HarborDto
                    {
                        Name = "Sprogø"
                    },
                    Price = 20,
                    TimeOfDeparture = DateTime.Now,
                    Duration = TimeSpan.FromHours(1)
                },
                ReturnTrip = new TripDto
                {
                    Origin = new HarborDto
                    {
                        Name = "Læsø"
                    },
                    Destination = new HarborDto
                    {
                        Name = "Sprogø"
                    },
                    Price = 20,
                    TimeOfDeparture = DateTime.Now,
                    Duration = TimeSpan.FromHours(1)
                },
                NumberOfPassengers = 1,
                Vehicle = null,
                Booker = new BookerDto
                {
                    Name = "Svend Bent",
                    Email = "svend@bent.dk"
                }
            };

            // Act
            _manager.CreateReservation(reservation);
        }

        [Test]
        public void FindPrettyReservation()
        {
            const string reservationNumber = "123456789";
            const string name = "Svend Bent";
            var reservationDto = _manager.FindReservation(reservationNumber, name);
            Assert.AreEqual("Læsø", reservationDto.OutboundTrip.Origin.Name, "The name of the origin harbor is not Læsø");
            Assert.AreEqual("Sprogø", reservationDto.OutboundTrip.Destination.Name, "The name of the destination harbor is not Sprogø");
        }

        [Test]
        [ExpectedException(typeof (ReservationNotFoundEto))]
        public void FindReservationWithWrongName()
        {
            const string reservationNumber = "123456789";
            const string name = "Bent Bent";
            _manager.FindReservation(reservationNumber, name);
        }

        [Test]
        [ExpectedException(typeof (ReservationNotFoundEto))]
        public void FindReservationWithWrongId()
        {
            const string reservationNumber = "14756";
            const string name = "Svend Bent";
            _manager.FindReservation(reservationNumber, name);
        }
    }
}