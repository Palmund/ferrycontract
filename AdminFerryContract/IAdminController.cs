﻿using System;
using System.Collections.Generic;
using Ferry.Admin.Contract.Dto;
using Ferry.Admin.Contract.Eto;

namespace Ferry.Admin.Contract
{
    public interface IAdminController
    {

        /// <summary>Add a departure to the persistence layer.</summary>
        /// <param name="departure">The departure to create</param>
        /// <returns>The created departure</returns>
        /// <exception cref="FerryException">thrown when something goes wrong. Message descripes the incident.</exception>
        DepartureDTO AddDeparture(DepartureDTO departure);

        /// <summary>gets a valid  list of routes that a ferry config with a configuration can sail</summary>
        /// <param name="ferryconfigid">The id of the configuration to look up</param>
        /// <returns>collection of valid routes that the ferry can sail</returns>
        /// <exception cref="FerryException">thrown when something goes wrong. Message descripes the incident.</exception>
        IEnumerable<RouteDTO> GetRoutesByFerryConfig(int ferryconfigid);

        /// <summary>Test method used only to get a list of ferry configs that will ease the system into the Admin "tilføj afgang" UC</summary>
        /// <returns>This of valid routes that the ferry can sail</returns>
        /// <exception cref="FerryException">thrown when something goes wrong. Message descripes the incident.</exception>
        IEnumerable<SimpleFerryConfigDTO> GetFerryConfigs();

        /// <summary>
        /// gets a valid list of departures given a date.
        /// </summary>
        /// <param name="time">the date on which to get departures from</param>
        /// <returns>a list of depatures for the given date</returns>
        IEnumerable<DepartureDTO> GetDepartures(DateTime time);

        /// <summary>
        /// gets a valud list of reservations given a depature
        /// </summary>
        /// <param name="departure">the depature dto to check</param>
        /// <returns>a list of reservations for the given depature</returns>
        IEnumerable<ReservationDTO> ShowReservations(DepartureDTO departure);
    }
}
