﻿namespace Ferry.Admin.Contract.Dto {
    public class SimpleFerryConfigDTO {
        /// <summary>
        /// Id uniquely identidfying the FerryConfig 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Output name for the UI
        /// </summary>
        public string Name { get; set; }
    }
}
