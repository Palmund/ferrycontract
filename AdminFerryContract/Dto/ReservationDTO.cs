﻿using System;

namespace Ferry.Admin.Contract.Dto
{
    public class ReservationDTO
    {
        public String ReservationId { get; set; }
        public String Booker { get; set; }
        public String Origin { get; set; }
        public String Destination { get; set; }
        public double Price { get; set; }
        public int NumberOfPassengers { get; set; }
    }
}
