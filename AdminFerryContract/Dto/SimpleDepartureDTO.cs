﻿namespace Ferry.Admin.Contract.Dto {
    public class SimpleDepartureDTO {
        public int Id { get; set; }
        public string Name{ get; set; }
    }
}
