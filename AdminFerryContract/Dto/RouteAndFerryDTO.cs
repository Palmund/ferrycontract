﻿using System.Collections.Generic;

namespace Ferry.Admin.Contract.Dto
{
    /// <summary> A Schedule transfer object</summary>
    public class RouteAndFerryDTO
    {
        /// <summary>
        /// List of all routes
        /// </summary>
        public IEnumerable<RouteDTO> Routes { get; set; }

        /// <summary>
        /// List of all ferry configurations
        /// </summary>
        public IEnumerable<SimpleFerryConfigDTO> FerryConfig { get; set; }

        /// <summary>List of departures occuring this month</summary>
        //public IEnumerable<DepartureDTO> Departures { get; set; }

    }
}
