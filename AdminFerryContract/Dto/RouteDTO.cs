﻿namespace Ferry.Admin.Contract.Dto {
    public class RouteDTO {
        /// <summary>
        /// Unique id, which is auto-generated for the RouteDTO
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name for the RouteDTO
        /// </summary>
        public string Origin { get; set; }
        public string Depature { get; set; }
    }
}
