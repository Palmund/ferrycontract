﻿using System;

namespace Ferry.Admin.Contract.Eto
{
    /// <summary> A Ferry transfer object</summary>
    public class FerryException : Exception
    {
        FerryException(string message) : base(message) { }
    }
}
